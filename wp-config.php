<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'pwp_dev');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '|+w6<-J|l3mLb3 9]{ZH4`+OLmqc83S9<t5#/pOWRoW:&xYbx<_N,6HBH&2E)nQY');
define('SECURE_AUTH_KEY',  '1WH8KW52poKV2f*rOQ_7tA=EmDy*3d()yD=NqfbBD:#LjJYew[+ubgQ~rkj^(?3k');
define('LOGGED_IN_KEY',    '[O 1m!3U29>;q@%~-JK(r5F^d4cl]}A#do5$Z){>!%~t--pCc=+q=S`*k10 twCn');
define('NONCE_KEY',        'TY9D((r5Mp@NEf+lG8cu=K[a4:&f@~c~nsers{,H,)VQMjqn=K/jEvu# MGUbJ>~');
define('AUTH_SALT',        '8>0}VF6CH2aj*5|Nt>Q&zseKa;ijsemUyQz<BG2O+$ewKsv2zUI|T.)k5bRv&W]m');
define('SECURE_AUTH_SALT', 'B(2V4Qx 5!Ch-`#y.Au0v/>A0BcY(ot[Wa5y[jk8V0q4MT:/Ge^cZvAi@HO6;#nx');
define('LOGGED_IN_SALT',   'So!$^*S#`R9z[=AbdkW_%A+Tlc~:CC<asH5Dn]@Cm~NKxi/Ag GJNYe:/GLR}1`p');
define('NONCE_SALT',       '!-A6I*8``s+qaDZ)zlm50ZZk^E1?>=jIKvx%BG]/z]Pe*XNyqigdgFQcu)+*I8jy');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
