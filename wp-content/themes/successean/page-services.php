<?php get_header(); ?>
<div class="serv-wrapper">
	<div class="serv-header">
		<h2>We offer the following services...</h2>
	</div>
	
	<div class="servy-scroll" style="background-image:url(<?php echo get_template_directory_uri(); ?>/img/city-park.jpg" alt="City Park");>


			<?php
	  			$args = array(
	    		'post_type' => 'our_services'
	    		);
	  			$products = new WP_Query( $args );
	  				if( $products->have_posts() ) {
	    			while( $products->have_posts() ) {
	      		$products->the_post();
			?>

		<div class="servy-box">
			<h3><?php the_field('service'); ?></h3>
			<p><?php the_field('short_text'); ?></p>
			<img class="box-image" src="<?php the_field('image'); ?>" alt="Insurance">
		</div>

		<?php
			}
				}
			else {
			echo 'No Services Found';
			}
		?>

</div>

<?php get_footer(); ?>

<script>
$( ".servy-scroll" ).scrollTop( 300 );
</script>