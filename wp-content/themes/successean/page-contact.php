<?php get_header(); ?>
<div class="contact-wrapper">
	<div class="contact-section-left">
		<div class="contact-section-info"> 
			<h4>Reach Out To Us</h4>
			<h2>Do you have questions or comments?</h2>
			<img src="<?php echo get_template_directory_uri(); ?>/img/contact-tel.png" alt="Telephone">
			<div class="contact-numbers">
				<h4>Call Us</h4>
				<p>212 721 3883</p>
				<h4>Fax Us</h4>
				<p>212 721 5660</p>
			</div>
		</div>

		<div class="contact-section-form">
			<h2>Message us</h2>
			<h3>A representative from our team will reply within 48 hours</h3>
			<?php echo do_shortcode('[contact-form-7 id="4" title="Contact form 1"]'); ?>
		</div>
	</div>

	<div class="contact-section-right">
		<img src="<?php echo get_template_directory_uri(); ?>/img/store-img-1.jpg" alt="Store Image">
		<img src="<?php echo get_template_directory_uri(); ?>/img/store-img-2.jpg" alt="Store Image">
		<img src="<?php echo get_template_directory_uri(); ?>/img/store-img-3.jpg" alt="Store Image">
	</div>
</div>

<?php get_footer(); ?>